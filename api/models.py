from django.db import models
from django.dispatch import receiver


class Student(models.Model):
    name = models.CharField(max_length=20)
    roll = models.IntegerField()
    city = models.CharField(max_length=30)
