import requests
import json

URL = "http://127.0.0.1:8000/studentapi/"

def get_data(id = None):
    data = {}
    if id is not None:
        data  = {'id' : id}
    json_data = json.dumps(data)
    headers = {'content-Type' : 'application/json'}
    r = requests.get(url = URL, headers = headers, data = json_data)
    data = r.json()
    print(data)

def post_data():
    data = {
        'name' : 'Ravi',
        'roll' : 110,
        'city' : 'Mumbai'
    }
    json_data = json.dumps(data)
    headers = {'content-Type' : 'application/json'}
    r = requests.post(url = URL, headers = headers, data = json_data)
    data = r.json()
    print(data)

def update_data():
    data = {
        'id' : 6,
        # 'name' : 'Abhi',
        # 'roll' : 104,
        'city' : 'Chennai' 
    }
    json_data = json.dumps(data)
    headers = {'content-Type' : 'application/json'}
    r = requests.put(url = URL, headers = headers, data = json_data)
    data = r.json()
    print (data)

def delete_data():
    data = {'id': 6}
    json_data = json.dumps(data)
    headers = {'content-Type' : 'application/json'}
    r = requests.delete(url = URL, headers = headers, data = json_data)
    data = r.json()
    print(data)

# get_data()
# post_data()
# update_data()
# delete_data()
get_data()